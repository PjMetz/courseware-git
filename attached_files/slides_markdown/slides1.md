---
title: 
- Git Workflow
author:
- Évariste Galois
---

# Introduction

*Git* is a **distributed version-control** system for tracking changes in files and coordinating work on those files among multiple people.
Distributed means that all users have a copy of the repository instead of a centralized version on a server.

![what-is-git](https://i.imgur.com/rxpXW0j.png)

# Git Workflow

- Create an issue
- Create a branch to tackle that issue
- Push commits
- Open a Merge Request
- Ask from feedback from your colleagues
- Deploy and merge once it's approved!


