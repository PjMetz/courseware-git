---
title: Quiz 1
nav_order: 2
---

{% quiz %}

---
primaryColor: steelblue
secondaryColor: '#e8e8e8'
textColor: black
shuffleQuestions: false
shuffleAnswers: true
locale: en
---

# Python Lists

What is the value of `x[2]`?

> Python lists are mutable!

```python
x = [2, 3, 4]
x[2] = 4
print(x[2])
```

- [ ] 1
- [ ] 2
- [ ] 3
- [x] 4

{% endquiz %}