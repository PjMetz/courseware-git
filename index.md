---
title: Home
nav_order: 1
description: "Courseware as Code fork used to teach Git to a beginner class"
permalink: /
---

# Courseware as Code

This section is designed to help you understand Git as part of a larger 
course on app development. 

{% attach_file {"file_name": "slides_markdown/slides1.pdf", "title":"slide-test"} %}

{% attach_file {"file_name": "slides_markdown/slides1.md", "title":"slide-test"} %}


This template started out as a fork of [just-the-docs](https://pmarsceill.github.io/just-the-docs/) and supports the same features.